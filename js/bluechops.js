Drupal.behaviors.Drupalorg = function () {
  var $element = $('body:not(.drupalorg-front) #edit-search-theme-form-1'),
    value = $element.siblings('label').text();

  // Add focus/blur label behavior to search box.
  $element.bind('focus', function () {
    if ($element.val() === value) {
      $element.val('').addClass('has-value');
    }
  });
  $element.bind('blur', function () {
    if ($element.val() === '' || $element.val() === value) {
      $element.val(value).removeClass('has-value');
    }
    else {
      $element.addClass('has-value');
    }
  });
  $element.trigger('blur');

  // Move right column out of the way of wide tables used on API.drupal.org and
  // forums.
  if ($('.one-sidebar #main').length === 1) {
    var delta = 0;
    $('#main table').each(function() {
      delta = Math.max(delta, $(this).width());
    });
    delta -= $('#column-left').width();
    if (delta > 0) {
      $('#page-inner').width($('#page-inner').width() + delta);
      $('#main').width($('#main').width() + delta);
    }
  }
};

<?php

/**
 * Preprocessor for page.tpl.php template file.
 */
function bluechops_preprocess_page(&$variables) {
  // Add HTML tag name for title tag.
  $variables['site_name_element'] = $variables['is_front'] ? 'h1' : 'div';

  // Add variable for site status message.
  $variables['drupalorg_site_status'] = filter_xss_admin(variable_get('drupalorg_site_status', FALSE));
}

/**
 * Implementation of template_preprocess_node().
 */
function bluechops_preprocess_node(&$variables) {
  $classes = array('node', 'clearfix');
  if (isset($variables['classes'])) {
    $classes = array_merge($classes, $variables['classes']);
  }
  if ($variables['node']->sticky) {
    $classes[] = 'sticky';
  }
  if (!$variables['node']->status) {
    $classes[] = 'node-unpublished';
  }
  if ($variables['node']->type) {
    $classes[] = 'node-type-' . $variables['node']->type;
  }

  foreach ($variables['node']->taxonomy as $term) {
    if (isset($term->rendered)) {
      unset($variables['taxonomy']['taxonomy_term_' . $term->tid]);
    }
  }
  if (!isset($variables['node']->links)) {
    $variables['node']->links = array();
  }
  if (!isset($variables['node']->extra_footer)) {
    $variables['node']->extra_footer = '';
  }
  $variables['links_and_terms'] = bluechops_linksterms($variables['node']->links, $variables['taxonomy'], $variables['type'], $variables['node']->extra_footer);

  if ($variables['readmore'] && !$variables['page']) {
    $variables['content'] .= l(t('Read more'), 'node/'. $variables['nid'], array('attributes' => array('rel' => 'nofollow')));
  }
  
  // News source tags
  $variables['source_tags'] = '';
  if ($variables['node']->type == 'forum') {
    $tags = bluechops_term_links_by_vocab($variables['node']->taxonomy);
    if (isset($tags[32])) {
      $variables['source_tags'] = '<span class="source-tags image-tags">';
      foreach ($tags[32] as $tid => $tag) {
        $variables['source_tags'] .= '<span class="source-tag-'.$tid.'">'. $tag.'</span>';
      }
      $variables['source_tags'] .= '</span>';
    }
  }

  $variables['classes'] = implode(' ', $classes);
}

/**
 * Custom function to generate combined $links and $terms output
 *
 * @return string
 *   HTML for the links or terms of a node ($links), with extra information if
 *   provided..
 */
function bluechops_linksterms($links, $terms, $node_type, $extra = NULL) {
  $output = '';

  foreach ($terms as $key => $term) {
    if ($term['href'] === 'taxonomy/term/903') {
      $terms[$key]['href'] = 'planet';
    }
  }
  $links_rendered = bluechops_linksterms_render($links, 'links');
  $terms_rendered = bluechops_linksterms_render($terms, 'terms');

  if (!empty($links_rendered)) {
    $output .= '<span class="links">'. implode(' &sdot; ', $links_rendered) .'</span>';
    
    // If both links and terms will be built, add a separator between the two spans
    if (!empty($terms_rendered)) {
      $output .= ' <span class="separator">&sdot;</span> ';
    }
  }

  if (!empty($terms_rendered)) {
    $output .= '<span class="terms">'. t('Categories') .': '. implode(', ', $terms_rendered) .'</span>';
  }

  if (!empty($extra)) {
    $output .= $extra;
  }

  if ($output) {
    $output = '<div class="node-footer">'. $output .'</div>';
  }

  return $output;
}

function bluechops_linksterms_render($links, $type) {
  $output = array();

  if (count($links) > 0) {
    if ($type == 'links') {
      // Remove unnecessary links
      unset($links['node_read_more']);
    }

    foreach ($links as $key => $link) {
      // Automatically add a class to each link.
      if (isset($link['attributes']) && isset($link['attributes']['class'])) {
        $link['attributes']['class'] .= ' ' . $key;
      }
      else {
        $link['attributes']['class'] = $key;
      }

      // Is this link an actual link?
      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output[] = l($link['title'], $link['href'], $link);
      }
      // Some links are actually not links. We wrap these in <span> for adding title and class attributes.
      else if (!empty($link['title'])) {
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output[] = '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }
    }
  }

  return $output;
}

/**
 * function bluechops_term_links_by_vocab()
 * @param array $tags An array of taxonomy term objects
 *
 * @return array An array of taxonomy term links ordered keyed by vocabulary
 */
function bluechops_term_links_by_vocab($tags) {
  
  if (count($tags) >= 1) {
    $links = array();
    foreach($tags as $term_id => $term) {
      $links[$term->vid][$term_id] = l($term->name, 'taxonomy/term/'. $term_id);
    }
    return $links;
  }
}

/**
 * Override theme_node_submitted().
 *
 * Changes the default "Submitted by..." text
 */
function bluechops_node_submitted($node) {
  return t('Posted by !username on %datetime',
    array(
      '!username' => theme('username', $node),
      '%datetime' => format_date($node->created, 'custom', 'F j, Y \a\t g:ia'),
    ));
}

/**
 * Implementation of template_preprocess_block().
 */
function bluechops_preprocess_block(&$variables) {
  // Add block classes
  $variables['block']->classes = 'block block-'. $variables['block']->module;
  if (function_exists("block_class")) {
    $variables['block']->classes .= ' '. block_class($variables['block']);
  }
}

/**
 * Implementation of template_preprocess_comment().
 *
function bluechops_preprocess_comment(&$vars) {
  static $zebra;
  $zebra = $zebra == 'odd' ? 'even':'odd';

  $classes = array();
  $classes[] = 'comment';
  $classes[] = $vars['status'];
  $classes[] = 'clearfix';
  $classes[] = 'comment-' . $zebra;
  if ($vars['comment']->new) {
    $classes[] = 'comment-new';
  }
  if (!empty($vars['picture'])) {
    $classes[] = 'comment-with-picture';
  }

  $vars['classes'] = implode(' ', $classes);
}

/**
 * Override theme_comment_submitted().
 *
 * Changes the default "Submitted by..." text
 */
function bluechops_comment_submitted($comment) {
  return t('Posted by !username on %datetime',
    array(
      '!username' => theme('username', $comment),
      '%datetime' => format_date($comment->timestamp, 'custom', 'F j, Y \a\t g:ia'),
    ));
}

/**
 * Override theme_help().
 *
 * Wraps help in <p> tags if they don't already exist.
 */
function bluechops_help() {
  if ($help = menu_get_active_help()) {
    if (substr($help, 0, 3) != '<p>') {
      $help = '<p>'. $help .'</p>';
    }

    return '<div class="help">'. $help .'</div>';
  }
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 * - Ensure an ID starts with an alpha character by optionally adding an 'id'.
 * - Replaces any character except A-Z, numbers, and underscores with dashes.
 * - Converts entire string to lowercase.
 *
 * @param $string
 *   The string
 * @return
 *   The converted string
 */
function bluechops_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}


/**
 *  TABLE TODO:decent documentation
 * captures the $cell data and reorder the call to telbe
 */
function bluechops_preprocess_forum_topic_list(&$variables) {
  global $forum_topic_list_header;

  // Create the tablesorting header.
  $ts = tablesort_init($forum_topic_list_header);
  $header = '';
  foreach ($forum_topic_list_header as $cell) {
    $cell = tablesort_header_span($cell, $forum_topic_list_header, $ts);
    $header .= _theme_table_cell($cell, TRUE);
  }
  $variables['header'] = $header;
}

/**
 * Disabled to save CPU
 */
function bluechops_forum_topic_navigation($node) {}

/**
 * Process variables for search-result.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $result
 * - $type
 *
 * @see search-result.tpl.php
 */
function bluechops_preprocess_search_result(&$variables) {
  $result = $variables['result'];
  $variables['url'] = check_url($result['link']);
  $variables['title'] = check_plain($result['title']);
  
  static $already_output = 0;
  if ($already_output < 2) {
    $already_output++;
  }

  if ($result['node']) {
    $variables['submitted'] = t('Posted by !username on %datetime',
    array(
      '!username' => $result['user'],
      '%datetime' => format_date($result['date'], 'custom', 'F j, Y \a\t g:ia'),
    ));
    unset($result['user']);
    unset($result['date']);
  }
  
  $matches = array();
  if (isset($result['extra']) && is_array($result['extra'])) {
    foreach ($result['extra'] as $key => $extra_item ) {
      // search into extras for attachments count and comments count
      preg_match('/(\d+) (attachment|comment)s?/', $extra_item, $matches);
      switch ($matches[2]) {
        case 'comment':
          $comments = $matches[1];
          unset($result['extra'][$key]);
          break;

        case 'attachment':
          $attachments = $matches[1];
          unset($result['extra'][$key]);
          break;

      }
    }
  }
  
  $info = array();
  if (isset($comments) && $result['node']->comment_count) {
    $info['comments'] = l(format_plural($comments, '1 comment', '@count comments'), $variables['url'], array('fragment' => 'comments', 'attributes' => array('class' => 'comments')));
  }
  if (!empty($result['type'])) {
    $info['type'] = check_plain($result['type']);
  }
  if (!empty($result['user'])) {
    $info['user'] = $result['user'];
  }
  if (!empty($result['date'])) {
    $info['date'] = format_date($result['date'], 'small');
  }
  if (isset($attachments)) {
    $info['upload'] = format_plural($attachments, '1 attachment', '@count attachments');
  }
  if (isset($result['extra']) && is_array($result['extra']) && $result['node']->comment) {
    $info = array_merge($info, $result['extra']);
  }
  // Check for existence. User search does not include snippets.
  $variables['snippet'] = isset($result['snippet']) ? $result['snippet'] : '';
  // Provide separated and grouped meta information..
  $variables['info_split'] = $info;
  $variables['info'] = implode(' - ', $info);
  // Provide alternate search result template.
  $variables['template_files'][] = 'search-result-'. $variables['type'];
}

/**
 * Process variables for search-results.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $results
 * - $type
 *
 * @see search-results.tpl.php
 */
//function bluechops_preprocess_search_results(&$variables) {
//  global $pager_total_items;
//  drupal_set_title(t('Found @count results containing the words: %words', array('@count' => $pager_total_items[0], '%words' => 'word')));
//  
//  $variables['search_results'] = '';
//  foreach ($variables['results'] as $result) {
//    $variables['search_results'] .= theme('search_result', $result, $variables['type']);
//  }
//  $variables['pager'] = theme('pager', NULL, 10, 0);
  // Provide alternate search results template.
//  $variables['template_files'][] = 'search-results-'. $variables['type'];
//}

/**
 * returns $header with a span around the link text
 */
function tablesort_header_span($cell, $header, $ts) {
  // Special formatting for the currently sorted column header.
  if (is_array($cell) && isset($cell['field'])) {
    $title = t('sort by @s', array('@s' => $cell['data']));
    if ($cell['data'] == $ts['name']) {
      $ts['sort'] = (($ts['sort'] == 'asc') ? 'desc' : 'asc');
      if (isset($cell['class'])) {
        $cell['class'] .= ' active';
      }
      else {
        $cell['class'] = 'active';
      }
      $table_sort_class = 'table-sort-'.$ts['sort'];
    }
    else {
      // If the user clicks a different header, we want to sort ascending initially.
      $ts['sort'] = 'asc';
      $table_sort_class = '';
    }

    if (!empty($ts['query_string'])) {
      $ts['query_string'] = '&'. $ts['query_string'];
    }
    $cell['data'] = l('<span class="'.$table_sort_class.'">' .$cell['data']. '</span>' .$image, $_GET['q'], array('attributes' => array('title' => $title), 'query' => 'sort='. $ts['sort'] .'&order='. urlencode($cell['data']) . $ts['query_string'], 'html' => TRUE));

    unset($cell['field'], $cell['sort']);
  }
  return $cell;
}



/**
 * Theme a compact project view/summary.
 *
 * Theming for Download and Extend tabs
 * $project (full node object)
 *
 * MB Prototypes show the D&E Project teasers with additional output to what
 * is ordinarily available from the Project module teaser. This theme
 * override sets the project summary theme function to output the data and
 * html so that it is closer to the prototype.
 *
 * https://infrastructure.drupal.org/drupal.org-style-guide/prototype/modules.html
 */
function bluechops_project_summary($project) {
  // Some initial variable work since we can't mess with project module build a
  // Project meta array to theme as an unordered list keeps things tidy and
  // provides theming flexibility.
  $project->meta = array();
  // Provide a themed 'posted by'
  $project->meta['submitted_by'] = array(
    'data' => theme('node_submitted', $project),
    'class' => 'submitted-by',
  );
  // Add the last changed data
  $project->meta['last_changed'] = array(
    'data' => t('Last changed: !interval ago', array('!interval' => format_interval(time() - $project->changed, 2))),
    'class' => 'last-changed',
  );
  // add a project wrapper class for the project being listed
  $wrapper_class = "project clearfix";
  if (!empty($project->class)) {
    $wrapper_class .= " " . $project->class;
  }
  $output = '<div class="' . $wrapper_class . '">';
  // output the main image
  $output .= $project->content['image_attach']['#value'];
  $output .= '<h2 class="title">'. l($project->title, "node/$project->nid") .'</h2>';
  if (!empty($project->meta)) {
    $list_meta = array('class' => 'project-meta flat');
    $output .= theme('item_list', $project->meta, NULL, 'ul', $list_meta);
  }
  $output .= $project->body;
  $output .= bluechops_linksterms($project->links, taxonomy_link('taxonomy terms', $project), 'project');
  $output .= '</div>';
  return $output;
}

/**
 * Override project_solr_category_page().
 *
 * Wraps category name and project names into grid divs and adds additional clearing div after 4th item.
 */
function bluechops_project_solr_category_page($project_type, $categories, $version_form = '') {
  $output = '';
  if (!empty($version_form)) {
    $output .= $version_form;
  }
  $i = 0;
  foreach ($categories as $tid => $category) {
    $class = "category";
    $output .= '<div class="' . $class . ' ' . $i . '">';
    $output .= '<h2 class="category">' . $category['title'] . '</h2>';
    $output .= theme('project_solr_category_list', $category['items']);
    $output .= '</div>';
    $i = ($i + 1) % 4;
  }
  return $output;
}
/*
 * Overiding theme_project_solr_category_list().
 *
 * Adds additional class "flat" for flat lists.
 */
function bluechops_project_solr_category_list($items = array()) {
  $output = '';
  if (!empty($items)) {
    $output .= theme('item_list', $items, NULL, "ul", array('class' => 'flat'));
  }
  return $output;
}

/**
 * Process variables for aggregator-item.tpl.php.
 *
 * @see aggregator-item.tpl.php
 */
function bluechops_preprocess_aggregator_item(&$variables) {
  $variables['source_date'] = format_date($variables['item']->timestamp, 'custom', 'F j, Y \a\t g:ia');

  foreach ($variables['categories'] as $key => $category) {
    if (strpos($category, 'class="active"') !== FALSE) {
      unset($variables['categories'][$key]);
    }
  }
}

function bluechops_preprocess_user_profile_category(&$variables) {
  if (isset($variables['element']['#attributes']['class'])) {
    $variables['element']['#attributes']['class'] .= ' clearfix';
  }
  else {
    $variables['element']['#attributes']['class'] = 'clearfix';
  }
  $variables['attributes'] = drupal_attributes($variables['element']['#attributes']);
}

/**
 * Show product-specific thank you messages.
 */
function bluechops_uc_cart_complete_sale($message) {
  $output = $message;

  if ($order = uc_order_load(intval($_SESSION['cart_order']))) {
    foreach ($order->products as $product) {
      $node = node_load($product->nid);
      if (isset($node->field_product_thanks)) {
        $output .= check_markup($node->field_product_thanks[0]['value'], $node->field_product_thanks[0]['format'], FALSE);
      }
    }
  }

  return $output;
}

/**
 * Implements hook_js_alter().
 *
 * This function allows for a whitelist of JavaScripts that should always appear in the <head>
 * of a document, and sets the scope of all other JS to 'footer'
 */
function bluechops_js_alter(&$javascript) {
  // Collect the scripts we want in to remain in the header scope.
  $header_scripts = array(
    'sites/all/libraries/modernizr/modernizr.min.js',
  );

  // Change the default scope of all other scripts to footer.
  // We assume if the script is scoped to header it was done so by default.
  foreach ($javascript as $key => &$script) {
    if ($script['scope'] == 'header' && !in_array($script['data'], $header_scripts)) {
      $script['scope'] = 'footer';
    }
  }
}

/**
 * Implements hook_form_alter().
 *
 * Add a placeholder attribute - let's move this back to drupalorg_search at some point
 */
function hook_form_search_block_form_alter(&$form, &$form_state, $form_id) {

  $form['search-wrapper']['query']['#attributes'] = array(
    'placeholder' => 'Search Drupal.org',
  );
}


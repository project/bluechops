<?php if ($content): ?>
  <div id="comments">
    <h2><?php print t('Comments'); ?></h2>
    <?php print $content; ?>
  </div>
<?php endif; ?>
